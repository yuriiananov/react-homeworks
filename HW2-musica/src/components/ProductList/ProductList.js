import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import ProductCard from "../ProductCard/ProductCard";

const ProductList = ({ addToCart, addToFavorites, favorites }) => {
    const [cards, setcards] = useState([]);
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch("./products-api.json");
                const result = await response.json();
                setIsLoaded(true);
                setcards(
                    result.map((card) => ({
                        id: card.id,
                        productName: card.productName,
                        productPrice: card.productPrice,
                        productColor: card.productColor,
                        productArticle: card.productArticle,
                        productImage: card.productImage,
                    }))
                );
            } catch (error) {
                setIsLoaded(true);
                setError(error);
            }
        };

        setTimeout(fetchData, 2000);
    }, []);

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div className="loader"></div>;
    } else {
        return (
            <div className="container product-list">
                <h2 className="product-list-title">
                    Internet-shop / Smartphones
                </h2>
                <ul className="product-list-items">
                    {cards.map((card) => (
                        <ProductCard
                            key={card.id}
                            id={card.id}
                            productName={card.productName}
                            productPrice={card.productPrice}
                            productImage={card.productImage}
                            productArticle={card.productArticle}
                            productColor={card.productColor}
                            addToCart={() => addToCart(card)}
                            addToFavorites={() => addToFavorites(card)}
                            favorites={favorites}
                        />
                    ))}
                </ul>
            </div>
        );
    }
};

ProductList.propTypes = {
    addToCart: PropTypes.func,
    addToFavorites: PropTypes.func,
    favorites: PropTypes.array,
};

ProductList.defaultProps = {
    addToCart: () => {},
    addToFavorites: () => {},
    favorites: [],
};

export default ProductList;
