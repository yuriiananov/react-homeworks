import React from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import favoritesIcon from "../../images/icon-star.png";
import favoritesIconActive from "../../images/icon-star-active.png";

const ProductCard = ({
    id,
    productName,
    productPrice,
    productImage,
    productArticle,
    productColor,
    addToCart,
    addToFavorites,
    favorites,
}) => {
    const favoriteProduct = favorites.find(
        (favProduct) => favProduct.id === id
    );
    const isFavorite = Boolean(favoriteProduct);

    const handleFavoriteClick = () => {
        addToFavorites({
            id,
            productName,
            productPrice,
            productImage,
            productArticle,
            productColor,
        });
    };

    return (
        <li className="product-card">
            <img
                className="product-image"
                src={productImage}
                alt={productName}
            />
            <div className="product-info">
                <h2 className="product-name">{productName}</h2>
                <p className="product-price">{productPrice.toLocaleString('uk-UA')} ₴</p>
                <p className="product-article">Article: {productArticle}</p>
                <p className="product-color">Color: {productColor}</p>
                <Button
                    className="add-to-cart"
                    text="Add to Cart 🛒"
                    onClick={() => addToCart()}
                />
                <Button
                    className={`add-to-favorites ${isFavorite ? "active" : ""}`}
                    onClick={handleFavoriteClick}
                >
                    <img
                        src={isFavorite ? favoritesIconActive : favoritesIcon}
                        alt="Add to favorites"
                    />
                </Button>
            </div>
        </li>
    );
};

ProductCard.propTypes = {
    id: PropTypes.number.isRequired,
    productName: PropTypes.string.isRequired,
    productPrice: PropTypes.number.isRequired,
    productImage: PropTypes.string.isRequired,
    productArticle: PropTypes.number.isRequired,
    productColor: PropTypes.string.isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
};

ProductCard.defaultProps = {
    id: 0,
    productName: "",
    productPrice: 0,
    productImage: "",
    productArticle: 0,
    productColor: "",
    addToCart: () => {},
    addToFavorites: () => {},
    favorites: [],
};

export default ProductCard;
