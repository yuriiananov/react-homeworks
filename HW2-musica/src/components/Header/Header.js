import React from "react";
import PropTypes from 'prop-types';
import starIconColor from "../../images/icon-star-active.png";
import cartIcon from "../../images/icon-shopping-cart.png";

const Header = ({ favorites, cart }) => {
    return (
        <header className="header-wrap">
            <div className="container">
                <div className="header-icons">
                    <div className="header-icon favorite-icon">
                        <a href="/favorites" className="icon-link">
                            <img src={starIconColor} alt="favorite-icon" />
                            <span className="icon-count">
                                {favorites.length}
                            </span>
                        </a>
                    </div>
                    <div className="header-icon cart-icon">
                        <a href="/cart" className="icon-link">
                            <img src={cartIcon} alt="cart-icon" />
                            <span className="icon-count">
                                Cart: {cart.length}
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </header>
    );
};

Header.propTypes = {
    favorites: PropTypes.array,
    cart: PropTypes.array,
};

Header.defaultProps = {
    favorites: [],
    cart: [],
};

export default Header;
