import React, { useState, useEffect } from "react";
import "./App.scss";
import Header from "./components/Header/Header";
import ProductList from "./components/ProductList/ProductList";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

const App = () => {
    const [cart, setCart] = useState(() => {
        const savedCart = localStorage.getItem("cart");
        return savedCart ? JSON.parse(savedCart) : [];
    });

    useEffect(() => {
        localStorage.setItem("cart", JSON.stringify(cart));
    }, [cart]);

    const [favorites, setFavorites] = useState(() => {
        const savedFavorites = localStorage.getItem("favorites");
        return savedFavorites ? JSON.parse(savedFavorites) : [];
    });

    useEffect(() => {
        localStorage.setItem("favorites", JSON.stringify(favorites));
    }, [favorites]);

    const addToCart = (product) => {
        setCart((prevCart) => [...prevCart, product]);
        openModal();
    };

    const addToFavorites = (product) => {
        setFavorites((prevFavorites) => {
            const productExists = prevFavorites.find(
                (favProduct) => favProduct.id === product.id
            );
            return productExists
                ? prevFavorites.filter((favProduct) => favProduct.id !== product.id)
                : [...prevFavorites, product];
        });
    };        

    const [isModalOpen, setIsModalOpen] = useState(false);

    const openModal = () => {
        setIsModalOpen(true);
    };

    const closeModal = () => {
        setIsModalOpen(false);
    };

    return (
        <>
            <Header favorites={favorites} cart={cart} />

            <ProductList
                addToCart={addToCart}
                addToFavorites={addToFavorites}
                favorites={favorites}
            />

            {isModalOpen && (
                <Modal
                    className="modal"
                    header="PRODUCT ADDED TO CART"
                    closeButton={false}
                    text=""
                    actions={
                        <>
                            <Button
                                backgroundColor="rgb(60, 74, 80)"
                                text="Continue shopping"
                                onClick={closeModal}
                            />
                            <Button
                                backgroundColor="rgb(60, 74, 80)"
                                text="Go to cart"
                                onClick={closeModal}
                            />
                        </>
                    }
                    onClose={closeModal}
                />
            )}
        </>
    );
};

export default App;
