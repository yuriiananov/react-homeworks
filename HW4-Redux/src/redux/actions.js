export const openModal = () => ({ type: "OPEN_MODAL" });
export const closeModal = () => ({ type: "CLOSE_MODAL" });
export const openCartModal = () => ({ type: "OPEN_CART_MODAL" });
export const closeCartModal = () => ({ type: "CLOSE_CART_MODAL" });
export const fetchDataSuccess = () => ({ type: "FETCH_DATA_SUCCESS" });

export const fetchData = () => {
    return async (dispatch) => {
        try {
            const response = await fetch("products-api.json");
            const result = await response.json();
            const data = result.map((card) => ({
                id: card.id,
                productName: card.productName,
                productPrice: card.productPrice,
                productColor: card.productColor,
                productArticle: card.productArticle,
                productImage: card.productImage,
            }));

            dispatch({ type: "FETCH_DATA", payload: data });
            dispatch(fetchDataSuccess());
        } catch (error) {
            dispatch({ type: "FETCH_DATA_ERROR", error: error.message });
        }
    };
};
