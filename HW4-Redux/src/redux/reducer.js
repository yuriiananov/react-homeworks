const initialState = {
    data: [],
    isModalOpen: false,
    isCartModalOpen: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "OPEN_MODAL":
            return { ...state, isModalOpen: true };
        case "CLOSE_MODAL":
            return { ...state, isModalOpen: false };
        case "OPEN_CART_MODAL":
            return { ...state, isCartModalOpen: true };
        case "CLOSE_CART_MODAL":
            return { ...state, isCartModalOpen: false };
        case "FETCH_DATA":
            return { ...state, data: action.payload };
        case "FETCH_DATA_SUCCESS":
            return { ...state, isLoaded: true };
        case "FETCH_DATA_ERROR":
            return { ...state, error: action.error };
        default:
            return state;
    }
};

export default reducer;
