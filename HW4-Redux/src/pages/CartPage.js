import React, { useState } from "react";
import Button from "../components/Button/Button";
import Modal from "../components/Modal/Modal";
import { useSelector, useDispatch } from "react-redux";
import { openCartModal, closeCartModal } from "../redux/actions";

const CartPage = ({ cart, removeFromCart }) => {
    const [productToRemove, setProductToRemove] = useState(null);

    const isCartModalOpen = useSelector((state) => state.isCartModalOpen);

    const dispatch = useDispatch();

    const handleRemoveFromCart = (product) => {
        setProductToRemove(product);
        dispatch(openCartModal());
    };

    const confirmRemoveFromCart = () => {
        removeFromCart(productToRemove.id);
        dispatch(closeCartModal());
    };

    return (
        <div className="container cart-list">
            {cart.length === 0 ? (
                <h2 className="cart-list-title">Cart is empty</h2>
            ) : (
                <>
                    <h2 className="cart-list-title">Cart:</h2>
                    <ul className="cart-list-items">
                        {cart.map((product) => (
                            <li className="cart-card" key={product.id}>
                                <img
                                    className="cart-card-image"
                                    src={product.productImage}
                                    alt={product.productName}
                                />
                                <div className="cart-card-info">
                                    <h2 className="cart-card-name">
                                        {product.productName}
                                    </h2>
                                    <p className="cart-card-price">
                                        Price:{" "}
                                        {product.productPrice.toLocaleString(
                                            "uk-UA"
                                        )}{" "}
                                        ₴
                                    </p>
                                    <p className="cart-card-article">
                                        Article: {product.productArticle}
                                    </p>
                                    <p className="cart-card-color">
                                        Color:&nbsp;
                                        <span
                                            style={{
                                                backgroundColor:
                                                    product.productColor,
                                                display: "inline-block",
                                                width: "20px",
                                                height: "20px",
                                            }}
                                        ></span>
                                    </p>
                                    <Button
                                        className="cart-card-delete"
                                        text="🗑 Delete from Cart"
                                        backgroundColor={
                                            "rgba(60, 143, 146, 0.7)"
                                        }
                                        onClick={() =>
                                            handleRemoveFromCart(product)
                                        }
                                    />
                                </div>
                            </li>
                        ))}
                        {isCartModalOpen && (
                            <Modal
                                className="modal"
                                header="DELETE PRODUCT FROM CART"
                                closeButton={false}
                                text=""
                                actions={
                                    <>
                                        <Button
                                            text="Delete"
                                            backgroundColor={"rgb(60, 74, 80)"}
                                            onClick={confirmRemoveFromCart}
                                        />
                                    </>
                                }
                                onClose={() => dispatch(closeCartModal())}
                            />
                        )}
                    </ul>
                </>
            )}
        </div>
    );
};
export { CartPage };
