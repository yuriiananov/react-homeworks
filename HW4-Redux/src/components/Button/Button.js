import React from "react";
import PropTypes from "prop-types";

const Button = ({ backgroundColor, text, onClick, className, children }) => {
    return (
        <button
            className={className}
            style={{ backgroundColor }}
            onClick={onClick}
        >
            {children}
            {text}
        </button>
    );
};

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
    children: PropTypes.node,
};

Button.defaultProps = {
    text: "",
    onClick: () => {},
    className: "",
};

export default Button;
