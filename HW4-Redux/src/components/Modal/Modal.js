import React from "react";
import PropTypes from "prop-types";

const Modal = ({ className, header, closeButton, text, actions, onClose }) => {
    return (
        <div className={`${className}__overlay`} onClick={onClose}>
            <div
                className={`${className}`}
                onClick={(e) => e.stopPropagation()}
            >
                <div className={`${className}__header`}>
                    <h2 className={`${className}__title`}>{header}</h2>
                    {closeButton && (
                        <button
                            className={`${className}__close-button`}
                            onClick={closeButton}
                        >
                            Х
                        </button>
                    )}
                </div>
                <div className={`${className}__body`}>
                    <p className={`${className}__text`}>{text}</p>
                </div>
                <div className={`${className}__actions`}>{actions}</div>
            </div>
        </div>
    );
};

Modal.propTypes = {
    className: PropTypes.string,
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.node,
    onClose: PropTypes.func,
};

export default Modal;
