import React, { useState } from "react";
import "./App.scss";
import Button from "./components/Button";
import Modal from "./components/Modal";

function App() {
  const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);

  return (
    <div>
      <Button backgroundColor="blue" text="Open first modal" onClick={() => setIsFirstModalOpen(true)} />
      <Button backgroundColor="green" text="Open second modal" onClick={() => setIsSecondModalOpen(true)} />

      {isFirstModalOpen && (
        <Modal
          className="modal-first"
          header="Do you want to delete this file?"
          closeButton={() => setIsFirstModalOpen(false)}
          text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
          actions={
            <>
              <Button backgroundColor="rgb(177, 67, 63)" text="Ok" onClick={() => console.log("Ok button clicked")} />
              <Button backgroundColor="rgb(177, 67, 63)" text="Cancel" onClick={() => setIsFirstModalOpen(false)} />
            </>
          }
          onClose={() => setIsFirstModalOpen(false)}
        />
      )}

      {isSecondModalOpen && (
        <Modal
          className="modal-second"
          header="Oops... Server is under maintenance"
          closeButton={false}
          text="Apologies for the inconvenience. Please try again later."
          actions={
            <Button backgroundColor="rgb(117, 131, 124)" text="Try Again Later" onClick={() => setIsSecondModalOpen(false)} />
          }
          onClose={() => setIsSecondModalOpen(false)}
        />
      )}
    </div>
  );
}

export default App;