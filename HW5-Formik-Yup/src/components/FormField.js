export const FormField = ({
    label,
    type,
    id,
    name,
    value,
    onChange,
    onBlur,
    error,
    touched,
}) => (
    <div className="form-control">
        <label htmlFor={id}>{label}</label>
        <input
            type={type}
            id={id}
            name={name}
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            className={error && touched ? "error-input" : ""}
        />
        {error && touched && <p className="error-message">{error}</p>}
    </div>
);
