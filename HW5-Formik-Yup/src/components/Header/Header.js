import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import starIconColor from "../../images/icon-star-active.png";
import cartIcon from "../../images/icon-shopping-cart.png";

const HomeMenuItem = () => (
    <div className="header-icon home-icon">
        <NavLink to="/" className="icon-link">
            Home
        </NavLink>
    </div>
);

const FavoritesMenuItem = ({ favorites }) => (
    <div className="header-icon favorite-icon">
        <NavLink to="/favorites" className="icon-link">
            <img src={starIconColor} alt="favorite-icon" />
            <span className="icon-count">{favorites.length}</span>
        </NavLink>
    </div>
);

FavoritesMenuItem.propTypes = {
    favorites: PropTypes.array,
};

FavoritesMenuItem.defaultProps = {
    favorites: [],
};

const CartMenuItem = () => {
    const cart = useSelector((state) => state.cart);

    return (
        <div className="header-icon cart-icon">
            <NavLink to="/cart" className="icon-link">
                <img src={cartIcon} alt="cart-icon" />
                <span className="icon-count">Cart: {cart.length}</span>
            </NavLink>
        </div>
    );
};

const Header = ({ favorites }) => {
    const cart = useSelector((state) => state.cart);
    return (
        <header className="header-wrap">
            <div className="container">
                <nav className="header-icons">
                    <HomeMenuItem />
                    <FavoritesMenuItem favorites={favorites} />
                    <CartMenuItem cart={cart} />
                </nav>
            </div>
        </header>
    );
};

export { Header };
