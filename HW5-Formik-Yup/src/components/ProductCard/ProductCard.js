import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import favoritesIcon from "../../images/icon-star.png";
import favoritesIconActive from "../../images/icon-star-active.png";
import { useDispatch, useSelector } from "react-redux";
import { addToCart } from "../../redux/actions";

const ProductCard = ({
    id,
    productName,
    productPrice,
    productImage,
    productArticle,
    productColor,
    addToFavorites,
    favorites,
}) => {
    const [isInCart, setIsInCart] = useState(false);
    const cart = useSelector((state) => state.cart);
    const dispatch = useDispatch();

    useEffect(() => {
        setIsInCart(cart.some((cartItem) => cartItem.id === id));
    }, [cart, id]);

    const handleAddToCartClick = () => {
        if (!isInCart) {
            dispatch(
                addToCart({
                    id,
                    productName,
                    productPrice,
                    productImage,
                    productArticle,
                    productColor,
                })
            );
        }
    };

    const favoriteProduct = favorites.find(
        (favProduct) => favProduct.id === id
    );
    const isFavorite = Boolean(favoriteProduct);

    const handleFavoriteClick = () => {
        addToFavorites({
            id,
            productName,
            productPrice,
            productImage,
            productArticle,
            productColor,
        });
    };

    return (
        <li className="product-card">
            <img
                className="product-image"
                src={productImage}
                alt={productName}
            />
            <div className="product-info">
                <h2 className="product-name">{productName}</h2>
                <p className="product-price">
                    {productPrice.toLocaleString("uk-UA")} ₴
                </p>
                <p className="product-article">Article: {productArticle}</p>
                <p className="product-color">Color: {productColor}</p>
                <Button
                    className={`add-to-cart ${isInCart ? "disabled" : ""}`}
                    text={isInCart ? " ✔️ In Cart" : "Add to Cart 🛒"}
                    onClick={handleAddToCartClick}
                    disabled={isInCart}
                />
                <Button
                    className={`add-to-favorites ${isFavorite ? "active" : ""}`}
                    onClick={handleFavoriteClick}
                >
                    <img
                        src={isFavorite ? favoritesIconActive : favoritesIcon}
                        alt="Add to favorites"
                    />
                </Button>
            </div>
        </li>
    );
};

ProductCard.propTypes = {
    id: PropTypes.number.isRequired,
    productName: PropTypes.string.isRequired,
    productPrice: PropTypes.number.isRequired,
    productImage: PropTypes.string.isRequired,
    productArticle: PropTypes.number.isRequired,
    productColor: PropTypes.string.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
};

ProductCard.defaultProps = {
    id: 0,
    productName: "",
    productPrice: 0,
    productImage: "",
    productArticle: 0,
    productColor: "",
    addToFavorites: () => {},
    favorites: [],
};

export default ProductCard;
