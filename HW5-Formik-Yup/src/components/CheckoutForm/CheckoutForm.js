import React, { useCallback } from "react";
import { PatternFormat } from "react-number-format";
import { useFormik } from "formik";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { clearCart } from "../../redux/actions";
import { FormField } from "../FormField";
import { validationSchema } from "../../helpers/validationSchema";
import "./CheckoutForm.scss";

const initialValues = {
    firstName: "",
    lastName: "",
    age: "",
    address: "",
    phone: "",
};

const CheckoutForm = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const onSubmit = useCallback(
        (values, actions) => {
            const cartItems = JSON.parse(localStorage.getItem("cart"));
            const newOrder = {
                ...values,
                cartInfo: cartItems,
            };
            console.log("Order data:", newOrder);
            actions.resetForm();
            dispatch(clearCart());
            navigate("/");
        },
        [dispatch, navigate]
    );

    const {
        values,
        touched,
        handleBlur,
        handleSubmit,
        handleChange,
        errors,
        isValid,
        dirty,
        setFieldValue,
    } = useFormik({
        initialValues,
        validationSchema,
        onSubmit,
    });

    return (
        <div className="checkout-form">
            <h2>Checkout Product</h2>
            <form onSubmit={handleSubmit} autoComplete="off">
                <FormField
                    label="First Name"
                    type="text"
                    id="firstName"
                    name="firstName"
                    value={values.firstName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.firstName}
                    touched={touched.firstName}
                />
                <FormField
                    label="Last Name"
                    type="text"
                    id="lastName"
                    name="lastName"
                    value={values.lastName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.lastName}
                    touched={touched.lastName}
                />
                <FormField
                    label="Age"
                    type="number"
                    id="age"
                    name="age"
                    value={values.age}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.age}
                    touched={touched.age}
                />
                <FormField
                    label="Address"
                    type="text"
                    id="address"
                    name="address"
                    value={values.address}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.address}
                    touched={touched.address}
                />
                <PatternFormat
                    format="(###) ###-##-##"
                    mask="_"
                    name="phone"
                    value={values.phone}
                    onValueChange={({ formattedValue }) => {
                        setFieldValue("phone", formattedValue);
                    }}
                    onBlur={handleBlur}
                    customInput={FormField}
                    label="Mobile Phone"
                    id="phone"
                    error={errors.phone}
                    touched={touched.phone}
                />
                <button disabled={!isValid || !dirty} type="submit">
                    Checkout
                </button>
            </form>
        </div>
    );
};

export default CheckoutForm;
