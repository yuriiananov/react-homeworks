import React, { useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import "./App.scss";
import { Layout } from "./components/Layout";
import { FavoritesPage } from "./pages/FavoritesPage";
import { CartPage } from "./pages/CartPage";
import { ProductList } from "./components/ProductList/ProductList";

const App = () => {
    const [favorites, setFavorites] = useState(() => {
        const savedFavorites = localStorage.getItem("favorites");
        return savedFavorites ? JSON.parse(savedFavorites) : [];
    });

    useEffect(() => {
        localStorage.setItem("favorites", JSON.stringify(favorites));
    }, [favorites]);

    const handleFavoriteClick = (product) => {
        addToFavorites(product);
    };

    const addToFavorites = (product) => {
        setFavorites((prevFavorites) => {
            const productExists = prevFavorites.find(
                (favProduct) => favProduct.id === product.id
            );
            return productExists
                ? prevFavorites.filter(
                      (favProduct) => favProduct.id !== product.id
                  )
                : [...prevFavorites, product];
        });
    };

    return (
        <>
            <Routes>
                <Route path="/" element={<Layout favorites={favorites} />}>
                    <Route
                        index
                        element={
                            <ProductList
                                addToFavorites={addToFavorites}
                                favorites={favorites}
                            />
                        }
                    />
                    <Route
                        path="favorites"
                        element={
                            <FavoritesPage
                                favorites={favorites}
                                handleFavoriteClick={handleFavoriteClick}
                            />
                        }
                    />
                    <Route path="cart" element={<CartPage />} />
                </Route>
            </Routes>
        </>
    );
};

export default App;
