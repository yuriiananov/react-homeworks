import React from "react";
import { BrowserRouter } from "react-router-dom";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import store from "./redux/store";
import "./index.scss";
import App from "./App";
import { ProductViewProvider } from "./context/ProductViewContext";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <BrowserRouter>
                <ProductViewProvider>
                    <App />
                </ProductViewProvider>
            </BrowserRouter>
        </Provider>
    </React.StrictMode>
);
