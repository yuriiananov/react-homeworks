import reducer from "./reducer";

const initialState = {
    data: [],
    cart: [],
    isModalOpen: false,
    isCartModalOpen: false,
};

describe("reducer", () => {
    it("test OPEN_MODAL", () => {
        const action = { type: "OPEN_MODAL" };
        const expectedState = reducer(initialState, action);
        expect(expectedState).toEqual({ ...initialState, isModalOpen: true });
    });

    it("test CLOSE_MODAL", () => {
        const action = { type: "CLOSE_MODAL" };
        const expectedState = reducer(initialState, action);
        expect(expectedState).toEqual({ ...initialState, isModalOpen: false });
    });

    it("test OPEN_CART_MODAL", () => {
        const action = { type: "OPEN_CART_MODAL" };
        const expectedState = reducer(initialState, action);
        expect(expectedState).toEqual({
            ...initialState,
            isCartModalOpen: true,
        });
    });

    it("test CLOSE_CART_MODAL", () => {
        const action = { type: "CLOSE_CART_MODAL" };
        const expectedState = reducer(initialState, action);
        expect(expectedState).toEqual({
            ...initialState,
            isCartModalOpen: false,
        });
    });

    it("test FETCH_DATA", () => {
        const action = { type: "FETCH_DATA", payload: ["data1", "data2"] };
        const expectedState = reducer(initialState, action);
        expect(expectedState).toEqual({
            ...initialState,
            data: ["data1", "data2"],
        });
    });

    it("test ADD_TO_CART", () => {
        const product = { id: 1, name: "product1" };
        const action = { type: "ADD_TO_CART", product };
        const expectedState = reducer(initialState, action);
        expect(expectedState).toEqual({ ...initialState, cart: [product] });
    });

    it("test REMOVE_FROM_CART", () => {
        const product = { id: 1, name: "product1" };
        const initialStateWithProduct = { ...initialState, cart: [product] };
        const action = { type: "REMOVE_FROM_CART", productId: 1 };
        const expectedState = reducer(initialStateWithProduct, action);
        expect(expectedState).toEqual({ ...initialState, cart: [] });
    });

    it("test CLEAR_CART", () => {
        const action = { type: "CLEAR_CART" };
        const expectedState = reducer(initialState, action);
        expect(expectedState).toEqual({ ...initialState, cart: [] });
    });
});
