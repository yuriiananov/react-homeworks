import { render } from "@testing-library/react";
import { FavoritesPage } from "./FavoritesPage";

describe("Favorites Page snapshot", () => {
    it("renders with empty favorites", () => {
        const { asFragment } = render(<FavoritesPage favorites={[]} />);
        expect(asFragment()).toMatchSnapshot();
    });

    it("renders with favorites", () => {
        const favorites = [
            {
                id: "1",
                productImage: "image.jpg",
                productName: "Product 1",
                productPrice: 100,
                productArticle: "Article 1",
                productColor: "red",
            },
        ];
        const { asFragment } = render(<FavoritesPage favorites={favorites} />);
        expect(asFragment()).toMatchSnapshot();
    });
});
