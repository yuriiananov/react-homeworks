import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import Modal from "../components/Modal/Modal";
import Button from "../components/Button/Button";

describe("render Modal component in Cart Page", () => {
    it("renders the Modal header and calls onClick when the 'Delete' button is clicked", () => {
        const confirmRemoveFromCart = jest.fn();

        const { getByText } = render(
            <Modal
                header="DELETE PRODUCT FROM CART"
                actions={
                    <Button text="Delete" onClick={confirmRemoveFromCart} />
                }
            />
        );

        fireEvent.click(getByText("Delete"));

        expect(confirmRemoveFromCart).toHaveBeenCalled();
        expect(getByText("DELETE PRODUCT FROM CART")).toBeInTheDocument();
    });
});
