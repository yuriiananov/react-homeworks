import React from "react";
import { Outlet } from "react-router-dom";
import { Header } from "./Header/Header";

const Layout = ({ favorites }) => {
    return (
        <>
            <Header favorites={favorites} />
            <Outlet />
        </>
    );
};

export { Layout };
