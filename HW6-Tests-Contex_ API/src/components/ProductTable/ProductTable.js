import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import "./ProductTable.scss";
import Button from "../Button/Button";
import favoritesIcon from "../../images/icon-star.png";
import favoritesIconActive from "../../images/icon-star-active.png";
import { useDispatch, useSelector } from "react-redux";
import { addToCart } from "../../redux/actions";

export const ProductTable = ({
    id,
    productName,
    productPrice,
    productImage,
    productArticle,
    productColor,
    addToFavorites,
    favorites,
}) => {
    const [isInCart, setIsInCart] = useState(false);
    const cart = useSelector((state) => state.cart);
    const dispatch = useDispatch();

    useEffect(() => {
        setIsInCart(cart.some((cartItem) => cartItem.id === id));
    }, [cart, id]);

    const handleAddToCartClick = () => {
        if (!isInCart) {
            dispatch(
                addToCart({
                    id,
                    productName,
                    productPrice,
                    productImage,
                    productArticle,
                    productColor,
                })
            );
        }
    };

    const favoriteProduct = favorites.find(
        (favProduct) => favProduct.id === id
    );
    const isFavorite = Boolean(favoriteProduct);

    const handleFavoriteClick = () => {
        addToFavorites({
            id,
            productName,
            productPrice,
            productImage,
            productArticle,
            productColor,
        });
    };
    return (
        <tr>
            <td className="product-table__image">
                <img src={productImage} alt={productName}></img>
            </td>
            <td>{productArticle}</td>
            <td>{productName}</td>

            <td style={{ color: "red" }}>
                {productPrice.toLocaleString("uk-UA")} ₴
            </td>
            <td>
                <span
                    style={{
                        backgroundColor: productColor,
                        display: "inline-block",
                        width: "30px",
                        height: "30px",
                    }}
                ></span>
            </td>

            <td>
                <span className="product-table__actions">
                    <Button
                        className={`add-to-cart ${isInCart ? "disabled" : ""}`}
                        text={isInCart ? " ✔️ In Cart" : "Add to Cart 🛒"}
                        onClick={handleAddToCartClick}
                        disabled={isInCart}
                    />
                    <Button
                        className={`add-to-favorites ${
                            isFavorite ? "active" : ""
                        }`}
                        onClick={handleFavoriteClick}
                    >
                        <img
                            src={
                                isFavorite ? favoritesIconActive : favoritesIcon
                            }
                            alt="Add to favorites"
                        />
                    </Button>
                </span>
            </td>
        </tr>
    );
};

ProductTable.propTypes = {
    id: PropTypes.number.isRequired,
    productName: PropTypes.string.isRequired,
    productPrice: PropTypes.number.isRequired,
    productImage: PropTypes.string.isRequired,
    productArticle: PropTypes.number.isRequired,
    productColor: PropTypes.string.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
};

ProductTable.defaultProps = {
    id: 0,
    productName: "",
    productPrice: 0,
    productImage: "",
    productArticle: 0,
    productColor: "",
    addToFavorites: () => {},
    favorites: [],
};

export default ProductTable;
