import { render, fireEvent, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { useDispatch } from "react-redux";
import reducer from "../../redux/reducer";
import ProductCard from "./ProductCard";

jest.mock("react-redux", () => ({
    ...jest.requireActual("react-redux"),
    useDispatch: jest.fn(),
}));

const mockProduct = {
    id: 1,
    productName: "Test Product",
    productPrice: 100,
    productImage: "test-image.jpg",
    productArticle: "Test Article",
    productColor: "Red",
    addToFavorites: jest.fn(),
    favorites: [],
};

const store = createStore(reducer);
store.getState().cart = [];

describe("ProductCard", () => {
    it("calls handleAddToCartClick when Add to Cart button is clicked", () => {
        const dispatch = jest.fn();
        useDispatch.mockReturnValue(dispatch);

        render(
            <Provider store={store}>
                <ProductCard {...mockProduct} />
            </Provider>
        );

        fireEvent.click(screen.getByText("Add to Cart 🛒"));
        expect(dispatch).toHaveBeenCalled();
    });

    it("calls handleFavoriteClick when Add to Favorites button is clicked", () => {
        render(
            <Provider store={store}>
                <ProductCard {...mockProduct} />
            </Provider>
        );

        fireEvent.click(screen.getByAltText("Add to favorites"));
        expect(mockProduct.addToFavorites).toHaveBeenCalled();
    });
});
