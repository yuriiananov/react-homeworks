import "./Switch.scss";
import { useProductView } from "../../context/ProductViewContext";

const Switch = () => {
    const { viewType, toggleViewType } = useProductView();
    return (
        <label className="switch">
            <input
                type="checkbox"
                onChange={() => toggleViewType()}
                checked={viewType}
            />
            <span className="slider round" />
        </label>
    );
};

export default Switch;
