import Modal from "./Modal";
import { render, fireEvent } from "@testing-library/react";

describe("Modal component", () => {
    it("modal onClose func", () => {
        const onClose = jest.fn();

        const { container } = render(
            <Modal className="test" onClose={onClose} />
        );

        const overlay = container.querySelector(".test__overlay");
        fireEvent.click(overlay);
        expect(onClose).toHaveBeenCalled();
    });

    it("modal closeButton func", () => {
        const closeButton = jest.fn();

        const { container } = render(
            <Modal className="test" closeButton={closeButton} />
        );

        const closeButtonElement = container.querySelector(
            ".test__close-button"
        );
        fireEvent.click(closeButtonElement);
        expect(closeButton).toHaveBeenCalled();
    });
});
