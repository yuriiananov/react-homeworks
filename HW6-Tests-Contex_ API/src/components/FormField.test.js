import { render } from "@testing-library/react";
import { FormField } from "./FormField";

describe("FormField snapshot", () => {
    it("renders without error", () => {
        const { asFragment } = render(
            <FormField
                label="Test Label"
                type="text"
                id="testId"
                name="testName"
                value="testValue"
                onChange={() => {}}
                onBlur={() => {}}
                error=""
                touched={false}
            />
        );
        expect(asFragment()).toMatchSnapshot();
    });

    it("renders with error", () => {
        const { asFragment } = render(
            <FormField
                label="Test Label"
                type="text"
                id="testId"
                name="testName"
                value="testValue"
                onChange={() => {}}
                onBlur={() => {}}
                error="Test error message"
                touched={true}
            />
        );
        expect(asFragment()).toMatchSnapshot();
    });
});
