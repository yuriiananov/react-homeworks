import Button from "./Button";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

describe("Button component tests", () => {
    it("button onClick event", () => {
        const onClick = jest.fn();
        const text = "Click me";
        render(<Button onClick={onClick} text={text} />);
        const button = screen.getByText(text);
        fireEvent.click(button);
        expect(onClick).toHaveBeenCalledTimes(1);
    });

    it("correct button text", () => {
        const text = "Click me";
        const { getByText } = render(<Button text={text} />);
        expect(getByText(text)).toBeInTheDocument();
    });

    it("button with className", () => {
        const className = "test-class";
        const { container } = render(<Button className={className} />);
        expect(container.firstChild).toHaveClass(className);
    });

    it("button background color", () => {
        const backgroundColor = "blue";
        const { container } = render(
            <Button backgroundColor={backgroundColor} />
        );
        expect(container.firstChild).toHaveStyle(
            `background-color: ${backgroundColor}`
        );
    });
});
