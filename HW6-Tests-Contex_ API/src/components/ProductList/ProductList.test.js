import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import { Link, MemoryRouter } from "react-router-dom";

describe("render Modal component in Product List", () => {
    it("correct render the Modal header", () => {
        const { getByText } = render(<Modal header="PRODUCT ADDED TO CART" />);

        expect(getByText("PRODUCT ADDED TO CART")).toBeInTheDocument();
    });

    it("renders the Modal buttons", () => {
        const handleClick = jest.fn();

        const { getByText } = render(
            <MemoryRouter>
                <Modal
                    actions={
                        <>
                            <Button
                                text="Continue shopping"
                                onClick={handleClick}
                            />
                            <Button>
                                <Link>Go to cart</Link>
                            </Button>
                        </>
                    }
                />
            </MemoryRouter>
        );

        fireEvent.click(getByText("Continue shopping"));

        expect(handleClick).toHaveBeenCalled();

        expect(getByText("Continue shopping")).toBeInTheDocument();
        expect(getByText("Go to cart")).toBeInTheDocument();
    });
});
