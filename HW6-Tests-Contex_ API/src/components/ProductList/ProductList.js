import React, { useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import PropTypes from "prop-types";
import ProductCard from "../ProductCard/ProductCard";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import Switch from "../Switch/Switch";
import { useProductView } from "../../context/ProductViewContext";
import { closeModal, fetchData } from "../../redux/actions";
import { useSelector, useDispatch } from "react-redux";
import { ProductTable } from "../ProductTable/ProductTable";

const ProductList = ({ addToFavorites, favorites }) => {
    const isModalOpen = useSelector((state) => state.isModalOpen);
    const location = useLocation();
    const dispatch = useDispatch();
    const data = useSelector((state) => state.data);
    const error = useSelector((state) => state.error);
    const isLoaded = useSelector((state) => state.isLoaded);
    const { viewType } = useProductView();

    useEffect(() => {
        dispatch(closeModal());
    }, [location.pathname, dispatch]);

    useEffect(() => {
        dispatch(fetchData());
    }, [dispatch]);

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div className="loader"></div>;
    } else {
        return (
            <div className="container product-list">
                <Switch />
                <h2 className="product-list-title">
                    Internet-shop / Smartphones
                </h2>

                {!viewType ? (
                    <ul className="product-list-items">
                        {data.map((card) => (
                            <ProductCard
                                key={card.id}
                                id={card.id}
                                productName={card.productName}
                                productPrice={card.productPrice}
                                productImage={card.productImage}
                                productArticle={card.productArticle}
                                productColor={card.productColor}
                                addToFavorites={() => addToFavorites(card)}
                                favorites={favorites}
                            />
                        ))}
                    </ul>
                ) : (
                    <table className="product-table">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Article</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Color</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((card) => (
                                <ProductTable
                                    key={card.id}
                                    id={card.id}
                                    productName={card.productName}
                                    productPrice={card.productPrice}
                                    productImage={card.productImage}
                                    productArticle={card.productArticle}
                                    productColor={card.productColor}
                                    addToFavorites={() => addToFavorites(card)}
                                    favorites={favorites}
                                />
                            ))}
                        </tbody>
                    </table>
                )}

                {isModalOpen && (
                    <Modal
                        className="modal"
                        header="PRODUCT ADDED TO CART"
                        closeButton={false}
                        text=""
                        actions={
                            <>
                                <Button
                                    backgroundColor="rgb(60, 74, 80)"
                                    text="Continue shopping"
                                    onClick={() => dispatch(closeModal())}
                                />
                                <Button backgroundColor="rgb(60, 74, 80)">
                                    <Link
                                        to="/cart"
                                        style={{
                                            color: "inherit",
                                            textDecoration: "inherit",
                                        }}
                                    >
                                        Go to cart
                                    </Link>
                                </Button>
                            </>
                        }
                        onClose={() => dispatch(closeModal())}
                    />
                )}
            </div>
        );
    }
};

ProductList.propTypes = {
    addToFavorites: PropTypes.func,
    favorites: PropTypes.array,
};

ProductList.defaultProps = {
    addToFavorites: () => {},
    favorites: [],
};

export { ProductList };
