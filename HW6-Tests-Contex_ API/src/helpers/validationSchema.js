import * as Yup from "yup";

export const validationSchema = Yup.object({
    firstName: Yup.string()
        .trim()
        .min(3, "* Must be 3 characters or more")
        .max(15, "* Must be 10 characters or less")
        .required("* Required field"),
    lastName: Yup.string()
        .trim()
        .min(3, "* Must be 3 characters or more")
        .max(20, "* Must be 15 characters or less")
        .required("* Required field"),
    age: Yup.number()
        .integer("* Must be an integer number")
        .positive("* Must be a positive number")
        .min(18, "* Must be 18 years or older")
        .max(60, "* Must be 60 years or younger")
        .required("* Required field"),
    address: Yup.string().required("* Required field"),
    phone: Yup.string()
        .required("* Required field")
        .matches(/\(\d{3}\) \d{3}-\d{2}-\d{2}/, "* Phone number is not valid"),
});
