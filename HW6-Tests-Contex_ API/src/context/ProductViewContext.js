import { createContext, useState, useContext, useEffect } from "react";

const ProductViewContext = createContext(undefined);

export const ProductViewProvider = ({ children }) => {
    const [viewType, setViewType] = useState(
        () => JSON.parse(localStorage.getItem("viewType")) || false
    );

    const toggleViewType = () => {
        setViewType(!viewType);
    };

    useEffect(() => {
        localStorage.setItem("viewType", JSON.stringify(viewType));
    }, [viewType]);

    return (
        <ProductViewContext.Provider value={{ viewType, toggleViewType }}>
            {children}
        </ProductViewContext.Provider>
    );
};

export const useProductView = () => useContext(ProductViewContext);
