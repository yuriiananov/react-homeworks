import React from 'react'
import { Outlet } from 'react-router-dom'
import { Header } from './Header/Header';

const Layout = ({ favorites, cart }) => {
    return (
        <>
            <Header favorites={favorites} cart={cart} />
            <Outlet />
        </>
    );
}

export { Layout };