import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import ProductCard from "../ProductCard/ProductCard";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import { openModal, closeModal } from "../../helpers/helpersModal";

const ProductList = ({ cart, addToCart, addToFavorites, favorites }) => {
    const [cards, setcards] = useState([]);
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [isModalOpen, setIsModalOpen] = useState(false);

    const handleAddToCart = (product) => {
        addToCart(product);
        openModal(setIsModalOpen);
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch("./products-api.json");
                const result = await response.json();
                setIsLoaded(true);
                setcards(
                    result.map((card) => ({
                        id: card.id,
                        productName: card.productName,
                        productPrice: card.productPrice,
                        productColor: card.productColor,
                        productArticle: card.productArticle,
                        productImage: card.productImage,
                    }))
                );
            } catch (error) {
                setIsLoaded(true);
                setError(error);
            }
        };

        setTimeout(fetchData, 500);
    }, []);

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div className="loader"></div>;
    } else {
        return (
            <div className="container product-list">
                <h2 className="product-list-title">
                    Internet-shop / Smartphones
                </h2>
                <ul className="product-list-items">
                    {cards.map((card) => (
                        <ProductCard
                            key={card.id}
                            id={card.id}
                            productName={card.productName}
                            productPrice={card.productPrice}
                            productImage={card.productImage}
                            productArticle={card.productArticle}
                            productColor={card.productColor}
                            addToCart={() => handleAddToCart(card)}
                            addToFavorites={() => addToFavorites(card)}
                            favorites={favorites}
                            cart={cart}
                        />
                    ))}
                    {isModalOpen && (
                        <Modal
                            className="modal"
                            header="PRODUCT ADDED TO CART"
                            closeButton={false}
                            text=""
                            actions={
                                <>
                                    <Button
                                        backgroundColor="rgb(60, 74, 80)"
                                        text="Continue shopping"
                                        onClick={() =>
                                            closeModal(setIsModalOpen)
                                        }
                                    />
                                    <Button backgroundColor="rgb(60, 74, 80)">
                                        <Link
                                            to="/cart"
                                            style={{
                                                color: "inherit",
                                                textDecoration: "inherit",
                                            }}
                                        >
                                            Go to cart
                                        </Link>
                                    </Button>
                                </>
                            }
                            onClose={() => closeModal(setIsModalOpen)}
                        />
                    )}
                </ul>
            </div>
        );
    }
};

ProductList.propTypes = {
    addToCart: PropTypes.func,
    addToFavorites: PropTypes.func,
    favorites: PropTypes.array,
};

ProductList.defaultProps = {
    addToCart: () => {},
    addToFavorites: () => {},
    favorites: [],
};

export { ProductList };
