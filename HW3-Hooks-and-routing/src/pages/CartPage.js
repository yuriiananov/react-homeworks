import React from "react";
import { useState } from "react";
import Button from "../components/Button/Button";
import Modal from "../components/Modal/Modal";
import { openModal, closeModal } from "../helpers/helpersModal";

const CartPage = ({ cart, removeFromCart }) => {
    const [productToRemove, setProductToRemove] = useState(null);
    const [isModalOpen, setIsModalOpen] = useState(false);

    const handleRemoveFromCart = (product) => {
        setProductToRemove(product);
        openModal(setIsModalOpen);
    };

    const confirmRemoveFromCart = () => {
        removeFromCart(productToRemove.id);
        closeModal(setIsModalOpen);
    };

    return (
        <div className="container cart-list">
            {cart.length === 0 ? (
                <h2 className="cart-list-title">Cart is empty</h2>
            ) : (
                <>
                    <h2 className="cart-list-title">Cart:</h2>
                    <ul className="cart-list-items">
                        {cart.map((product) => (
                            <li className="cart-card" key={product.id}>
                                <img
                                    className="cart-card-image"
                                    src={product.productImage}
                                    alt={product.productName}
                                />
                                <div className="cart-card-info">
                                    <h2 className="cart-card-name">
                                        {product.productName}
                                    </h2>
                                    <p className="cart-card-price">
                                        Price:{" "}
                                        {product.productPrice.toLocaleString(
                                            "uk-UA"
                                        )}{" "}
                                        ₴
                                    </p>
                                    <p className="cart-card-article">
                                        Article: {product.productArticle}
                                    </p>
                                    <p className="cart-card-color">
                                        Color:&nbsp;
                                        <span
                                            style={{
                                                backgroundColor:
                                                    product.productColor,
                                                display: "inline-block",
                                                width: "20px",
                                                height: "20px",
                                            }}
                                        ></span>
                                    </p>
                                    <Button
                                        className="cart-card-delete"
                                        text="🗑 Delete from Cart"
                                        backgroundColor={
                                            "rgba(60, 143, 146, 0.7)"
                                        }
                                        onClick={() =>
                                            handleRemoveFromCart(product)
                                        }
                                    />
                                </div>
                            </li>
                        ))}
                        {isModalOpen && (
                            <Modal
                                className="modal"
                                header="DELETE PRODUCT FROM CART"
                                closeButton={false}
                                text=""
                                actions={
                                    <>
                                        <Button
                                            text="Delete"
                                            backgroundColor={"rgb(60, 74, 80)"}
                                            onClick={confirmRemoveFromCart}
                                        />
                                    </>
                                }
                                onClose={() => closeModal(setIsModalOpen)}
                            />
                        )}
                    </ul>
                </>
            )}
        </div>
    );
};
export { CartPage };
