import React from "react";
import Button from "../components/Button/Button";
import favoritesIconActive from "../images/icon-star-active.png";

const FavoritesPage = ({ favorites, handleFavoriteClick }) => {
    return (
        <div className="container cart-list">
            {favorites.length === 0 ? (
                <h2 className="cart-list-title">Favorites is empty</h2>
            ) : (
                <>
                    <h2 className="cart-list-title">Favorites:</h2>
                    <ul className="cart-list-items">
                        {favorites.map((product) => (
                            <li className="cart-card" key={product.id}>
                                <img
                                    className="cart-card-image"
                                    src={product.productImage}
                                    alt={product.productName}
                                />
                                <div className="cart-card-info">
                                    <h2 className="cart-card-name">
                                        {product.productName}
                                    </h2>
                                    <p className="cart-card-price">
                                        Price:{" "}
                                        {product.productPrice.toLocaleString(
                                            "uk-UA"
                                        )}{" "}
                                        ₴
                                    </p>
                                    <p className="cart-card-article">
                                        Article: {product.productArticle}
                                    </p>
                                    <p className="cart-card-color">
                                        Color:&nbsp;
                                        <span
                                            style={{
                                                backgroundColor:
                                                    product.productColor,
                                                display: "inline-block",
                                                width: "20px",
                                                height: "20px",
                                            }}
                                        ></span>
                                    </p>
                                    <Button
                                        className="add-to-favorites-star active"
                                        onClick={() =>
                                            handleFavoriteClick(product)
                                        }
                                    >
                                        <img
                                            src={favoritesIconActive}
                                            alt="Add to favorites"
                                        />
                                    </Button>
                                </div>
                            </li>
                        ))}
                    </ul>
                </>
            )}
        </div>
    );
};
export { FavoritesPage };
